"use strict";

class _WSMSG {
    
    constructor(data,event="") {
        this.type =  "unknow";
        this.status = 500; //error
        this.data = data;
        this.event = event;
    }
    setStatus(code) {
        this.status = code;
    }
    toClient() {
        return JSON.stringify({ type:this.type, status:this.status,data : this.data,event:this.event});
    }
}

class WSResponse extends _WSMSG {
    constructor(data,event="") {
        super();
        this.type =  "response";
        this.status = 200; //error
        this.data = data;
        this.event = event;
    }
}

class WSError extends _WSMSG {
    constructor(data,event="") {
        super();
        this.type =  "error";
        this.status = 300; //error
        this.data = data;
        this.event = event;
    }
}
 

module.exports = {WSResponse,WSError};