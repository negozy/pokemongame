var laziMessage = setTimeout(() => {},1);
var audioWin = new Audio('/audio/win.mp3');
var audioLose = new Audio('/audio/lose.wav');
var app = new Vue({
    el: '#app',
    data() {
        return { 
            showModal: false,
            gameReady : false,
            canIAttack: false,
            challenge: {
                n : 7,
                m : 7,
                r : 49
            },
            modalFrame: "attack", // youWin, youLose
            score : 0,
            gain : 0,
            message: 'Gioco dei Pokemon' , 
            name : "",
            pokemons : [],
            pokemons_win : [],
            pokemonAvversario: {url : "/images/default.png"},
            opposite : {
                default_pokemon : true,
                pokemons : [],
                win : false,
                points: 0
            },
            timer : {
                seconds : 0,
                passed : 0,
                running : false,
                clock : {},
                perc : 100
            },
            winnerGifs : []
        };
    },
    methods: {
        onInput(e) {
            clearTimeout(laziMessage);
            laziMessage = setTimeout(() => {
                sendData2WS("setName",e.target.innerText);
            }, 2000);
        },
        activate(el) {
            if(el.active) {
                el.active = false;
            } else {
                this.resetCards();
                el.active = true;
                this.startGame();
            }
            
        },
        loadPokemons(pokemons,oppositePokemons) {
            this.pokemons = pokemons.map(el => {
                el.active = false;
                return el;
            });
            // init opposite
            this.opposite.pokemons = oppositePokemons;
            this.deployPokemon();
            
        },
        winPokemon() {
            console.log("WIN POKEMON");
            this.pokemons_win.push(this.pokemonAvversario);
            let newPokemon =  this.opposite.pokemons.shift();
            this.opposite.pokemons= this.opposite.pokemons.slice(1);
            this.deployPokemon(newPokemon);
        },
        getPokemonWins() {
            let pos = 0;
            return this.pokemons_win.map(e => {
                e.pos = pos+=20;
            });
        },
        getRandomWinnerGif() {   
            return "/images/winGif/"+this.winnerGifs[Math.floor(Math.random()*this.winnerGifs.length)];
        },
        setPlaysetStatus(ready,canIAttack=false) {
            this.gameReady = ready;
            this.canIAttack = canIAttack;
            if(canIAttack) {
                
            } else {
                
            }
        },
        oppositeSetPokemon(pokemon) {
            this.opposite = pokemon;
        },
        openModal() {
            this.showModal = true;
            this.startTimer(10);
        },
        closeModal() {
            this.showModal = false;
        },
        resolveWith(n) {
           if(n == this.challenge.m) {
               this.youWin();
           } else {
               this.youLose();
           }
        }, 
        startTimer(seconds) {
            this.timer.seconds = seconds;
            this.timer.passed = 0;
            this.timer.running= true;
            this.timer.clock = setInterval(() => {
                if(this.timer.seconds < this.timer.passed) {
                    this.timer.running= false;
                    this.expiredTime();
                    
                } else {
                    this.timer.passed +=0.100;
                    this.timer.perc = (this.timer.seconds != 0) ? 
                        (1 - (this.timer.passed/this.timer.seconds))*100 : 
                        100;
                    
                }
            },100);
        },
        expiredTime() {
            clearTimeout(this.timer.clock);
            this.youLose();
        },
        setWinnerGifs(vGIFS) {
            this.winnerGifs = vGIFS;
        },
        pauseGame() {
            setTimeout(() => {
                this.resetCards();
                clearTimeout(this.timer.clock);
                this.showModal = false;
            },5000);
        },
        youWin() {
            audioWin.play();
            this.pauseGame();
            this.showModal = true;
            this.modalFrame = "youWin";
            this.gain = Math.round(this.challenge.r* this.timer.perc/100);
            this.score += this.gain;
            this.pokemonAvversario.power -= this.gain;
            if(this.pokemonAvversario.power < 0) {
                this.winPokemon();
            }
        },
        youLose() {
            audioLose.play();
            clearTimeout(this.timer.clock);
            this.showModal = true;
            this.modalFrame = "youLose";
            this.pauseGame();
        },
        createChallenge() {
            let nmrs = [2,3,4,5,6,7,8,9];
            
            this.challenge.n = nmrs[Math.floor(Math.random()*nmrs.length)];
            this.challenge.m = nmrs[Math.floor(Math.random()*nmrs.length)];
            this.challenge.r = this.challenge.m * this.challenge.n;
        },
        deployPokemon(newPokemon) {
            console.log("DEPLOY POKEMON!!!!",this.opposite.pokemons);
            if(this.opposite.pokemons.length>0) {
                // check if is first run
                if(this.opposite.default_pokemon) {
                    console.log("SET FIRST POKEMON OPPOSITE");
                    this.pokemonAvversario = this.opposite.pokemons[0];
                    this.opposite.default_pokemon = false;
                } else {
                    this.pokemonAvversario = newPokemon;
                }
            } else {
                //you win!
            }
        },
        resetCards() {
            clearTimeout(this.timeOutGame);
            this.pokemons = this.pokemons.map(el => {el.active =false; return el});
        },
        startGame() {
            console.log("StartNewGame");
            setTimeout(() => {
                console.log("reset");
                //reset
                this.closeModal();
                this.canIAttack=false;
                this.modalFrame= "attack";

            },1000);
            this.timeOutGame = setTimeout(() => {
                console.log("set Game");
                //newgame
                this.createChallenge();
                this.openModal();
            },3000);
        }
      }
  })

