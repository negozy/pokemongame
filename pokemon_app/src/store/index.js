import { createStore } from 'vuex'
import pokemonData from '../assets/db.json';
function getRandomItems(items, n) {
  return items.sort(() => 0.5 - Math.random()).slice(0, n);
}

var audioWin = new Audio('/audio/win.mp3');
var audioLose = new Audio('/audio/lose.wav');
var audioGameOver = new Audio('/audio/GameOver.mp3');
export default createStore({
  state: {
    gain : 0,
    flippedCard:false,
    modalFrame: "close",
    challenge: {
      n: 1,
      m: 1,
      r: 1,
      mode: 2
    },
    timer: {
      
    },
    me: {
      pokemons: [],
      score: 0,
      wins: []
    },
    opposite: {
      pokemons: [],
      score: 0,
      wins: []
    }
  },
  getters: {
    getMe: state => state.me,
    getMyPokemon: state => state.me.pokemons.find(p => p.active),
    getOppositePokemon: state => position => {
      let p =state.opposite.pokemons[position];
      p.active = true; 
      return p;
    }
  },
  mutations: {
    initGame: state => {
      state.modalFrame = "close";

      let pokemonINgara = getRandomItems(pokemonData, 12);

      state.me = {
        pokemons: pokemonINgara.slice(0,6),
        score: 0,
        pokemon: null
      };
      state.opposite = {
        pokemons: pokemonINgara.slice(6,12),
        score: 0,
        pokemon: null
      };
    },
    startChallenge: function(state) {
      let nmrs = [2, 3, 4, 5, 6, 7, 8, 9];
      state.challenge.n = nmrs[Math.floor(Math.random() * nmrs.length)];
      state.challenge.m = nmrs[Math.floor(Math.random() * nmrs.length)];
      state.challenge.r = state.challenge.m * state.challenge.n;
      state.challenge.mode = (Math.random() < .5) ? 1 : 2;
      state.timer.seconds = 10;
      state.timer.passed = 0;
      state.timer.clock = setInterval(() => {
        if (state.timer.seconds < state.timer.passed) {
          //Timeout: you loose
          state.modalFrame="youLose";
          audioLose.play();
          this.commit('restartGame');
          clearInterval(state.timer.clock);
        } else {
          state.timer.passed += 0.100;
          state.timer.perc = (state.timer.seconds != 0) ? (1 - (state.timer.passed / state.timer.seconds)) * 100 : 100;
        }
      }, 100);
      state.modalFrame="attack";
    },
    youWin: function(state) {
      audioWin.play();
      let myPokemon = state.me.pokemons.find(p => p.active);
      let oppositePokemon = state.opposite.pokemons.find(p => p.active);
      let gain = Math.round(myPokemon.power*state.timer.perc/100);
      if(gain < 9) {
        gain = 10;
      }
      oppositePokemon.power -= gain;
      state.me.score += gain;
      state.gain = gain;
      clearInterval(state.timer.clock);
      state.modalFrame="youWin";
      //verifico se ho vinto la carta
      if(oppositePokemon.power<1 ) {
        this.commit("youWinAPokemon",oppositePokemon);
        if(!state.me.wins) {
          state.me.wins = []  
        }
        state.me.wins.push(oppositePokemon);
        let newSet = state.opposite.pokemons.filter(el => el.id != oppositePokemon.id);
        state.opposite.pokemons = newSet;
        if(newSet.length <= 0) {
          // YouWinGame
          this.commit('youWinGame');
        } else {
          this.commit('restartGame');  
        }
      } else {
        this.commit('restartGame');
      }
    },
    youLose: function(state) {
      let myPokemon = state.me.pokemons.find(p => p.active);
      let oppositePokemon = state.opposite.pokemons.find(p => p.active);
      let gain = Math.round(oppositePokemon.power*state.timer.perc/100);
      myPokemon.power -= gain;
      state.gain = -gain;
      audioLose.play();
      clearInterval(state.timer.clock);
      state.modalFrame="youLose";
      let remainsPokemon = state.me.pokemons.filter(p => p.power > 0);
      if(remainsPokemon.length>0) {
        this.commit('restartGame');
      } else {
        //YouLoseGame
        this.commit('youLoseGame');
      }
      
    },
    restartGame : state => {
      setTimeout(() => {
      state.modalFrame="close";
      state.flippedCard = false;
      state.me.pokemons = state.me.pokemons.map(el => {el.active =false; return el});
      },5000);
    },
    youWinGame : state => {
      state.modalFrame="winGame";
      state.flippedCard = false;
      state.me.pokemons = state.me.pokemons.map(el => {el.active =true; return el}); 
      
    },
    youLoseGame : state => {
      audioGameOver.play();
      state.modalFrame="loseGame";
      state.flippedCard = false;
      state.me.pokemons = state.me.pokemons.map(el => {el.active =true; return el}); 
      
    },
    youWinAPokemon : function(state,payload) {
      console.log("YouWin",payload,state);
    },
    youLoseAPokemon : function(state,payload) {
      console.log("YouLose",payload,state);
    }
  },
  actions: {
    startGame(context) {
      context.commit('initGame');
    }
  },
  modules: {
  }
})
