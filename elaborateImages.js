const srcFolder = './public/pokemon/';
const tmpFolder = './tmp_images/';
const fs = require('fs');
const sharp = require('sharp');

let pokemons = [];
fs.readdirSync(srcFolder).forEach(file => {
    pokemons.push(file);
});
let test = ["BWP_IT_BW59.png"];
async function extractName(file) {
    return new Promise((resolve,reject) => {
        sharp(srcFolder+file) //245 × 342
        .extract({ width: 130, height: 20, left: 50, top: 13 })
        .toFile(tmpFolder +"name."+ file)
        .then(function(new_file_info) {
            console.log("Image cropped and saved:",new_file_info);
            resolve(new_file_info);
        })
        .catch(function(err) {
            console.log("An error occured",err);
            reject(err);
        });
    });
}

async function extractPower(file) {
    return new Promise((resolve,reject) => {
        sharp(srcFolder+file) //245 × 342
        .extract({ width: 33, height: 20, left: 180, top: 13 })
        .toFile(tmpFolder +"power."+ file)
        .then(function(new_file_info) {
            console.log("Image cropped and saved:",new_file_info);
            resolve(new_file_info);
        })
        .catch(function(err) {
            console.log("An error occured",err);
            reject(err);
        });
    });
}
async function elaborateAll() {
    for(img of pokemons) {
        let power = await extractPower(img);
       // let name = await extractName(img);
        console.log(power);
    }
}

function createDB() {
    let data = pokemons.map(p => {
        return {
            id : p.split('.').slice(0, -1).join('.'),
            file : p,
            name : "",
            power : 0
        }
    });  
    let data2store = JSON.stringify(data);
    fs.writeFileSync('./data/db.json', data2store);
}

elaborateAll();
//createDB();


//extractPower(test[0]);