//const api_key = "0UTRbFtkMxAplrohufYco5IY74U8hOes";
import fetch from 'node-fetch';
import * as fs from 'fs';

const folder2Download = "./downloadedGifs/";
const url = "https://any-api.com:8443/https://api.giphy.com/v1/gifs/random?api_key=0UTRbFtkMxAplrohufYco5IY74U8hOes&tag="; //https://api.giphy.com/v1/gifs/random?tag=funny  
const downloadFile = (async (url, path) => {
    const res = await fetch(url);
    const fileStream = fs.createWriteStream(path);
    await new Promise((resolve, reject) => {
        res.body.pipe(fileStream);
        res.body.on("error", reject);
        fileStream.on("finish", resolve);
      });
  });
async function downloadOne() {
    return new Promise((resolve,reject) => {
        fetch(url+"funny", {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.text()).then(responseJson => {
            var item = JSON.parse(responseJson);
            console.log(item.data.images.original.url);
            let gifUrl = item.data.embed_url;
            resolve({name : gifUrl, url : item.data.images.original.url});
            //console.log(responseJson);
        }).catch(reject);
    });
}
async function downloadMany() {
    for (let i=0; i<100; i++) {
    await    downloadOne().then(file => {
            downloadFile(file.url,folder2Download+file.name.substring(24)+".gif");
        });
    }
}

downloadMany();