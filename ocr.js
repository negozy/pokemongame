const { createWorker } = require('tesseract.js');
const worker = createWorker();
const fs = require('fs');

const db = require("./data/db.json");
/*const db = [
  {
    "id": "BWP_IT_BW01",
    "file": "BWP_IT_BW01.png",
    "name": "",
    "power": 0
  },
  {
    "id": "BWP_IT_BW02",
    "file": "BWP_IT_BW02.png",
    "name": "",
    "power": 0
  },
  {
    "id": "BWP_IT_BW03",
    "file": "BWP_IT_BW03.png",
    "name": "",
    "power": 0
  }
];*/
const alpha = Array.from(Array(26)).map((e, i) => i + 65);
const Alphabet = alpha.map((x) => String.fromCharCode(x));
const alphabet = alpha.map((x) => String.fromCharCode(x).toLowerCase());
const allChars = alphabet.join("")+ Alphabet.join("");




(async () => {
  
  await worker.load();
  await worker.loadLanguage('eng');
  await worker.initialize('eng');
  await worker.setParameters({
    tessedit_char_whitelist: allChars,
  });

  for(pokemon of db) {
    //const { data: { text } } = await worker.recognize('./tmp_images/power.'+pokemon.file);
    const { data: { text } } = await worker.recognize('./tmp_images/name.'+pokemon.file);
    pokemon.name = text.trim();
    if(pokemon.name.includes(" ")) {
      let tmp_name = pokemon.name.split(" ");
      pokemon.name = tmp_name[0];
    }
    console.log(pokemon.name,pokemon.file);
    
  }
  
  await worker.terminate();
  let data2store = JSON.stringify(db);
  fs.writeFileSync('./data/db.json', data2store);
})();