"use strict";

const db = require("./data/db.json");

function getPokemons() {
    return db.map(el => {
        el.url = "/pokemon/"+el.file; 
        if(el.power == 0) {
            el.power = 100; // fix some pokemons 
        }
        return el; 
    });
}
class Pokemon {

    constructor(file) {
        this.url = "/pokemon/"+file;
        this.id = file.split('.').slice(0, -1).join('.');
    }
    toJSON() {
        return {url: this.url,id:this.id};
    }
}


function getRandomPokemons(items,n) {
    let r = [];
    for (let i=0; i<n;i++) {
        let p = items[Math.floor(Math.random() * items.length)];
        r.push(p);
    }
    return r;
}
module.exports = {Pokemon,getPokemons,getRandomPokemons};