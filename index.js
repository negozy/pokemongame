"use strict";
const uuid = require("uuid");
const fs = require('fs');
var P1;
var P2;
var staticServer = require('node-static');
var file = new (staticServer.Server)(__dirname + "/public");
const winnerGifs= [];
fs.readdirSync("./public/images/winGif/").forEach(file => {
  winnerGifs.push(file);
});
const { WSResponse, WSError } = require("./utils");
const { getPokemons, getRandomPokemons } = require("./pokemon");
// Optional. You will see this name in eg. 'ps' or 'top' command
process.title = 'tabelline-game';
// Port where we'll run the websocket server
var webSocketsServerPort = 3000;
// websocket and http servers
var webSocketServer = require('websocket').server;
var http = require('http');
/**
 * Global variables
 */
const updateConnection = (c, dt) => {
  let d = clients.get(c).data;
  if (d) {
    for (const [key, value] of Object.entries(dt)) {
      d[key] = value;
    }
    clients.set(c, d);
  }
  return d;
}
const listUserInfo = () => {
  clients.forEach((value) => {
    console.log(value);
  });
}
// list of currently connected clients (users)
var clients = new Map();
const all_pokemons = getPokemons();

/**
 * HTTP server
 */
var server = http.createServer(function (req, res) {
  //res.writeHead(200, {'Content-Type': 'text/plain'});
  file.serve(req, res);
  //res.end();

});
server.listen(webSocketsServerPort, function () {
  console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
});
/**
 * WebSocket server
 */
var wsServer = new webSocketServer({
  // WebSocket server is tied to a HTTP server. WebSocket
  // request is just an enhanced HTTP request. For more info 
  // http://tools.ietf.org/html/rfc6455#page-6
  httpServer: server
});
// This callback function is called every time someone
// tries to connect to the WebSocket server
wsServer.on('request', function (request) {

  //console.log((new Date()) + ' Connection from origin '+ request.origin + '.');
  // accept connection - you should check 'request.origin' to
  // make sure that client is connecting from your website
  // (http://en.wikipedia.org/wiki/Same_origin_policy)
  const connection = request.accept(null, request.origin);
  if (P1) {
    P2 = connection;
    clients.set(connection, {
      id: uuid.v4(),
      name: "",
      data: {
        pokemons: getRandomPokemons(all_pokemons, 6),
        oppositePokemons : getRandomPokemons(all_pokemons, 6),
      },
      opposite: P1
    });
    updateConnection(P1,{opposite:P2});
    var random_boolean = Math.random() < 0.5;
    P1.sendUTF(new WSResponse({name : clients.get(P2).name,id : clients.get(P2).id, canIAttack:random_boolean}, "playsetReady").toClient());
    P2.sendUTF(new WSResponse({name : clients.get(P1).name,id : clients.get(P1).id, canIAttack:!random_boolean}, "playsetReady").toClient());
    
  } else {
    P1 = connection;
    clients.set(connection, {
      id: uuid.v4(),
      name: "",
      data: {
        pokemons: getRandomPokemons(all_pokemons, 6),
        oppositePokemons : getRandomPokemons(all_pokemons, 6),
      },
      opposite: {}
    });
  }
  // we need to know client index to remove them on 'close' event

  listUserInfo();
  console.log((new Date()) + ' Connection accepted.');

  // user sent some message
  connection.on('message', function (message) {
    if (message.type === 'utf8') { // accept only text
      //console.log("message:",message.utf8Data, "from",clients.get(connection).id);
      try {
        let msg = JSON.parse(message.utf8Data);
        
      
        switch (msg.type) {
          case "setName":
            let customerData = updateConnection(connection, { name: msg.data });
            let pokemonsUser = {
              name: customerData.name,
              pokemons: customerData.pokemons,
              oppositePokemons : customerData.oppositePokemons,
              winnerGifs
            };
            console.log(customerData);
            connection.sendUTF(new WSResponse(pokemonsUser, "initUser").toClient());
            break
          default:
            connection.sendUTF(new WSError("Unknow message.type : " + msg.type).toClient());
        }
        listUserInfo();
      } catch (e) {
        connection.sendUTF(new WSError("Message was malformed : " + message.utf8Data + ":" + e).toClient());
      }
    }
  });

  // user disconnected
  connection.on('close', function (connection) {
    clients.delete(connection);
  });
  /*
  connection.on('message', function(message) {
    if (message.type === 'utf8') { // accept only text
    // first message sent by user is their name
     if (userName === false) {
        // remember user name
        userName = htmlEntities(message.utf8Data);
        // get random color and send it back to the user
        userColor = colors.shift();
        connection.sendUTF(
            JSON.stringify({ type:'color', data: userColor }));
        console.log((new Date()) + ' User is known as: ' + userName
                    + ' with ' + userColor + ' color.');
      } else { // log and broadcast the message
        console.log((new Date()) + ' Received Message from '
                    + userName + ': ' + message.utf8Data);
        
        // we want to keep history of all sent messages
        var obj = {
          time: (new Date()).getTime(),
          text: htmlEntities(message.utf8Data),
          author: userName,
          color: userColor
        };
        history.push(obj);
        history = history.slice(-100);
        // broadcast message to all connected clients
        var json = JSON.stringify({ type:'message', data: obj });
        for (var i=0; i < clients.length; i++) {
          clients[i].sendUTF(json);
        }
      }
    }
  });
  // user disconnected
  connection.on('close', function(connection) {
    if (userName !== false && userColor !== false) {
      console.log((new Date()) + " Peer "
          + connection.remoteAddress + " disconnected.");
      // remove user from the list of connected clients
      clients.splice(index, 1);
      // push back user's color to be reused by another user
      colors.push(userColor);
    }
  });*/
});